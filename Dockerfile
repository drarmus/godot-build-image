from debian:latest

WORKDIR app/godot

RUN apt-get update && \
    apt-get install git build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev \
    libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm -y

ENTRYPOINT ["scons", "-j8"]